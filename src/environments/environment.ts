// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    
      apiKey: "AIzaSyAaJIdSkfkGhp4sCWMyjKS4gLwLWntTde0",
      authDomain: "portfoliostore-fc780.firebaseapp.com",
      databaseURL: "https://portfoliostore-fc780.firebaseio.com",
      projectId: "portfoliostore-fc780",
      storageBucket: "portfoliostore-fc780.appspot.com",
      messagingSenderId: "149520854334",
      appId: "1:149520854334:web:43f7287d3e5c4e0224906f",
      measurementId: "G-KMD9NZLBXV"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
