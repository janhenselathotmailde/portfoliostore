import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { AboutUsComponent } from './shared/about-us/about-us.component';
import { CustommaterialModule } from './custommaterial.module';
import { SettingsComponent } from './settings/settings.component';
import { SetproductComponent } from './admin/setproduct.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { RegistrationComponent } from './auth/registration/registration.component';
import {LoginComponent} from './auth/login/login.component'
import { HomeComponent } from './home/home.component';
import {FirebaseserviceService} from './../app/admin/service/firebaseservice.service'
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { DropZoneDirective } from './drop-zone.directive';
import { FilesizePipe } from './filesize.pipe';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ReusableButtonComponent } from './reusable-button/reusable-button.component';






@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AboutUsComponent,
    SettingsComponent,
    SetproductComponent,
    RegistrationComponent,
    LoginComponent,
    HomeComponent,
    DropZoneDirective,
    FilesizePipe,
    ShoppingCartComponent,
    ReusableButtonComponent
    

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CustommaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase, 'portfoliostore'), // imports firebase/app needed for everything
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
   
  
  ],
  providers: [FirebaseserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
