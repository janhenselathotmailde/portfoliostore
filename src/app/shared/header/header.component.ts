import { Component, OnInit, Input } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { FirebaseserviceService } from '../../admin/service/firebaseservice.service';
import {Subscription} from 'rxjs';
import {Router,ActivatedRoute} from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from 'src/app/auth/auth.service';






@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

 
@Input() pageTitel: string;
@Input() iconTitel: string;
@Input() helpTitel: string;
userStatusColor="warn";
orders
totalvalue
subscription:Subscription;
counter=0;
user

  constructor(private db : AngularFireDatabase,private productApi:FirebaseserviceService,
    private route:ActivatedRoute,private router: Router,private afAuth: AngularFireAuth,private service:AuthService
    ) {
    this.orders=this.productApi.orders;
    this.productApi.totalSubject.subscribe(totalvalue=>{
      console.log(" got"  + totalvalue)
      this.totalvalue=totalvalue
    })
  }

  ngOnInit() {
    this.service.getLoggedinUser()
    .subscribe(user => {
      this.user = user;
    })

  }
  logOut() {
    this.service.logOut();
    this.router.navigate(['/login']);
  }

  

} 
