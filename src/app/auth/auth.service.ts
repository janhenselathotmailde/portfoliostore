import { Injectable } from '@angular/core';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { User, firestore, auth } from 'firebase';
import { FirebaseserviceService } from '../admin/service/firebaseservice.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // Für die Fehlermeldungen,falls sich ein User ein invalides Passwort etc ausgesucht hat

  private eventAuthError = new BehaviorSubject<string>("");
  eventAuthError$ = this.eventAuthError.asObservable();
  user:any;
  newUser: any;

  constructor(private afAuth: AngularFireAuth, private router: Router, private db: AngularFirestore) {
    
   }
   

   

  
   loginWithEmail( email: string, password: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .catch(error => {
        this.eventAuthError.next(error);
      })
      .then(userCredential => {
        if(userCredential) {
          this.router.navigate(['/home']);
        }
      })
  }

  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
    .then(() => {
      this.router.navigate(['login']);
    })
  }
  
  //store the information of the user in the firebase data base

  createUser(user) {
    this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then(userCredential => {
        this.newUser = user;
        console.log(userCredential);
        userCredential.user.updateProfile({
          displayName: user.firstName + '' + user.lastName
        });
        this.insertUserData(userCredential)
          .then(() => {
            this.router.navigate(['/home']);
          });
      })
      .catch(error => {
        this.eventAuthError.next(error);
      });
  }
  insertUserData(userCredential: firebase.auth.UserCredential) {
    return this.db.doc(`Users/${userCredential.user.uid}`).set({
      email: this.newUser.email,
      firstname: this.newUser.firstName,
      lastname: this.newUser.lastName,
      role: 'customer'
    })
  }

login(){
  console.log('redirectiong to google login provider');
  this.afAuth.auth.signInWithRedirect(new auth.GoogleAuthProvider());
}
getLoggedinUser(){
  return this.afAuth.authState;
}

logOut(){
  this.afAuth.auth.signOut();
}
  
}


