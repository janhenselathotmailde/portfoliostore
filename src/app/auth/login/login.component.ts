import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable, of } from 'rxjs';
import { User, auth, firestore } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user:firebase.User;

  constructor(private service:AuthService, private router: Router, private afAuth: AngularFireAuth, private afs: AngularFirestore
  ) { }

  ngOnInit() {

    
    this.service.getLoggedinUser()
    .subscribe(user=>{
      console.log(user);
      this.user=user;
    })
    
  }
  
  login(frm) {
    this.service.loginWithEmail(frm.value.email, frm.value.password);
  }

  loginGoogle() {
    console.log('Login...');
    this.service.login()
    this.router.navigate(['home']);
  }

  logOut(){
    this.service.logOut();
  }
  

  /* google login try 
async googleSignIn(){
  const provider= new auth.GoogleAuthProvider();
  const credential= await this.afAuth.auth.signInWithPopup(provider);
  return this.updateUserData(credential.user);

}
async signOut(){
  await this.afAuth.auth.signOut();
  return this.router.navigate(['/']);

}
  private updateUserData({uid,email,displayName,photoURL}:User){
    //sets user data to firestore on login
    const userRef: AngularFirestoreDocument<User> =this.afs.doc(`users/${uid}`);

    const data ={
      uid,
      email,
      displayName,
      photoURL,
    };
    return userRef.set(data, {merge: true});
  }
*/
}

