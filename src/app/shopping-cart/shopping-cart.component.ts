import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FirebaseserviceService } from '../admin/service/firebaseservice.service';
import { ShowOnDirtyErrorStateMatcher } from '@angular/material';
import { database } from 'firebase';




declare var paypal;
@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})


export class ShoppingCartComponent implements OnInit {

  //pay pal implementation
  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;
  // is the article paid?

  totalvalue;
  orders
  ProductData: any = [];
  displayOrders: Boolean;

  orderCompleted = []

  constructor(private productApi: FirebaseserviceService) {
    this.orders = this.productApi.orders;
    this.productApi.totalSubject.subscribe(totalvalue => {
      console.log(" got" + totalvalue)
      this.totalvalue = totalvalue
    })

  }

  product = {
    price: 777.77,
    description: 'used couch, decent condition',
    img: 'assets/couch.jpg'
  };

  paidFor = false;

  ngOnInit() {
    if (this.total() > 0 && !this.paidFor) {
      paypal
        .Buttons({
          createOrder: (data, actions) => {
            return actions.order.create({
              purchase_units: [
                {
                  description: this.product.description,
                  amount: {
                    currency_code: 'USD',
                    value: this.total(),
                  }
                }
              ]
            });
          },
          onApprove: async (data, actions) => {
            const order = await actions.order.capture();
            this.paidFor = true;
            console.log(order);
            this.productApi.createNewCustomerOrderConfirmationDetailList(order);
          },
          onError: err => {
            console.log(err);
          }
        })
        .render(this.paypalElement.nativeElement);
    }
  }
  total() {
    for (let o of this.orders) {
      return this.orders.reduce((total, item) => total + item.price * item.quantity, 0);
    }
  }





}
