export interface Product {

    $key: string;
   product_name: string;
   weight: number;
   price: string
   in_stock: string;
   image: string;

}
