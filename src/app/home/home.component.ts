import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router'
import { FirebaseserviceService } from '../admin/service/firebaseservice.service';
import { Product } from '../product';
import { MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { ImageserviceService } from '../imageservice.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // Url from the saved picture in Firestorage
  downloadURL: Observable<string>;
  // instances for the Product list
  dataSource: MatTableDataSource<Product>;
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // Product data from the commited form of adding a product  all informations from a specific product
  //the total cart value (quanitiy)
  totalcartvalue = 0;
  //Boolean to control apperence
  conditionToDisaply = false;
  // array for orders
  orders = [];
  // array for the products
  imageList: any[];
  rowIndexArray: any[];
  //total price of order
  totalPriceOfOrder: any [];








  // snackbar for the information of a product is added, element ref for the animaton of the gradient/ hover effect on buttons 
  constructor(private auth: AuthService,
    private router: Router,
    private productApi: FirebaseserviceService,
    public snackBar: MatSnackBar,
    private service: ImageserviceService,
  ) {

    this.service.getImageDetailList().snapshotChanges().subscribe(products => {
      products.forEach(item => {
        let a = item.payload.toJSON();
        a['$key'] = item.key;
        this.imageList.push(a as Product)
      })
      /* Data table */
      this.dataSource = new MatTableDataSource(this.imageList);
      /* Pagination */
      //     setTimeout(() => {
      //     this.dataSource.paginator = this.paginator;
      //     }, 0);
    })
  }

  user: firebase.User;

  ngOnInit() {

    /*
    this.auth.getUserState()
      .subscribe(user => {
        this.user = user;
      })
*/
    this.service.imageDetailList.snapshotChanges().subscribe(
      list => {
        this.imageList = list.map(item => { return item.payload.val();});
        this.rowIndexArray = Array.from(Array(Math.ceil(this.imageList.length / 3)).keys());
      }
    );

  }



  login() {
    this.router.navigate(['/login']);
  }

  

  register() {
    this.router.navigate(['/register']);
  }
  // add to card function

  addToCart(indexValue, propductId) {

    //snackbar notification

    if (this.imageList[indexValue].quantity != 0) {
      this.snackBar.open("  Added to Cart", this.imageList[indexValue].product_name, { duration: 2000, })
    }
    else if (this.imageList[indexValue].quantity == 0) {
      this.snackBar.open("Product Unavailable", this.imageList[indexValue].product_name, { duration: 2000, })
    }
    //conditions to display +  and - buttons
    //Add to cart 
    this.totalcartvalue += 1;
    console.log(this.totalcartvalue + "cart value ")
    let count = 1
    let push = true
    console.log(indexValue + "   " + this.imageList[indexValue].$key + this.orders)
    if (this.imageList[indexValue].quantity == 0) {

      return

    }

    for (let ords of this.productApi.orders) {
      if (ords.indexVal == indexValue) {
        console.log("index value already")
        ords.quantity++;
        push = false
        this.conditionToDisaply = true

      }
    }
    if (push) {
      this.productApi.orders.push({ "indexVal": indexValue, "quantity": count, "productKey": this.imageList[indexValue].$key, "productName": this.imageList[indexValue].product_name, "price": this.imageList[indexValue].price, "image": this.imageList[indexValue].imageUrl, "in_stock": this.imageList[indexValue].in_stock })
      this.conditionToDisaply = true
    }
    this.imageList[indexValue].quantity--;
    this.productApi.sendTotal(this.totalcartvalue);

    //send to service

  }
  removeFromCart(indexValue, propductId) {
    this.totalcartvalue -= 1;
    this.imageList[indexValue].quantity++;
    for (let ord of this.productApi.orders) {
      if (ord.indexVal == indexValue) {
        ord.quantity -= 1
      }
    }
    this.snackBar.open("  Removed From Cart  ", this.imageList[indexValue].name, { duration: 2000, })
    this.productApi.sendTotal(this.totalcartvalue)
    console.log(this.totalcartvalue + "heeey")
  }
  //get quanity

  getQuantity(i) {
    for (let orders of this.productApi.orders) {
      if (orders.indexVal == i) {
        return orders.quantity
      }
    }
  }
  //button to see orders
  show() {
    //  this.productservice.sendOrders(this.orders ) 
    console.log(this.productApi.orders)

  }
}
