import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Product } from 'src/app/product';
import { Subject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class FirebaseserviceService {

  orders=[]
  totalPriceOfOrder: any [];


public totalSubject = new Subject()
public orderSubject = new Subject()

  productsRef: AngularFireList<any>;
  productRef: AngularFireObject<any>;

  conformationRef:AngularFireList<any>;

  constructor(private db: AngularFireDatabase,private afs: AngularFirestore ) { 
    
  }

  AddProduct(product: Product) {
    this.productsRef.push({
      product_name: product.product_name,
      weight: product.weight,
      price: product.price,
      in_stock: product.in_stock,
      image: product.image
    })
    .catch(error => {
      this.errorMgmt(error);
    })
  }
  GetProduct(id: string) {
    this.productRef = this.db.object('products-list/' + id);
    return this.productRef;
  }  
  GetProductList() {
    this.productsRef = this.db.list('products-list');
    return this.productsRef;
  }
  private errorMgmt(error) {
    console.log(error)
  }

  UpdateProduct(id, product: Product) {
    this.productRef.update({
      product_name: product.product_name,
      weight: product.weight,
      price: product.price,
      in_stock: product.in_stock,
    })
    .catch(error => {
      this.errorMgmt(error);
    })
  }

  DeleteProduct(id: string) {
    this.productRef = this.db.object('products-list/' + id);
    this.productRef.remove()
    .catch(error => {
      this.errorMgmt(error);
    })
  }

  sendTotal(totalVal)
    { 
      this.totalSubject.next(totalVal)
    }

    createNewCustomerOrderConfirmationDetailList(confirmationDetailList) {
      return this.db.list('ConfirmationDetailsList').push(confirmationDetailList);
    }

    getConfirmationDetailsList(){
      this.conformationRef= this.db.list('conformationDetailsList')
      return this.conformationRef;
    }

    
    
   
    
   
}
