import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FirebaseserviceService } from './service/firebaseservice.service';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import {  finalize } from 'rxjs/operators';
import * as firebase from 'firebase';
import { ImageserviceService } from '../imageservice.service';
import { fromValue } from 'long';

@Component({
  selector: 'app-setproduct',
  templateUrl: './setproduct.component.html',
  styleUrls: ['./setproduct.component.css']
})
export class SetproductComponent implements OnInit {

//new try for the upload and safe product form
formTemplate= new FormGroup({
  product_name: new FormControl(''),
  category: new FormControl('',Validators.required),
  imageUrl: new FormControl('',Validators.required),
  price: new FormControl('')
})

// instance for the default image which is shown at add a product
imageSrc:string ;

// selected image which is uploaded
selectedImage: any =null;

isSubmitted: boolean;

//Dropzone instance
  profileUrl: Observable<string | null>;
  task: AngularFireUploadTask;
  // Progress monitoring
  percentage: Observable<number>;

  snapshot: Observable<any>;
  // Download URL
  downloadURL: Observable<string>;

  // State for dropzone CSS toggling
  isHovering: boolean;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  
  editProductForm: FormGroup;
  productForm: FormGroup;

  toogleField: string;
  exampleForm: FormGroup;

  constructor(private fb: FormBuilder,
    public dialog: MatDialog,
    private router: Router,
    public firebaseService: FirebaseserviceService,
    private actRoute: ActivatedRoute,
    private productApi: FirebaseserviceService,
    private storage: AngularFireStorage,
     private db: AngularFirestore,
     private service:ImageserviceService) {
      const ref = this.storage.ref('test/');

     this.profileUrl = ref.getDownloadURL();
  
    var id = this.actRoute.snapshot.paramMap.get('id');
    this.productApi.GetProduct(id).valueChanges().subscribe(data => {
      this.editProductForm.setValue(data);

    })
  }


  ngOnInit() {

    this.toogleField = "searchMode";
    this.productApi.GetProductList();
    this.submitProductForm();
    this.updateProductForm();
    this.service.getImageDetailList();
    this.resetForm();

  }

  toogle(filter?) {

    if (!filter) { filter = "searchMode" }
    else { filter = filter; }
    this.toogleField = filter;
  }

  submitProduct() {
    if (this.productForm.valid) {
      this.productApi.AddProduct(this.productForm.value)
    }
  }
  submitProductForm() {
    this.productForm = this.fb.group({
      product_name: ['', [Validators.required]],
      weight: ['', [Validators.required]],
      price: ['', [Validators.required]],
      in_stock: [''],
      image: ['']
      

    })
  }
  getFilterData(filters) {

  }

  public handleError = (controlName: string, errorName: string) => {
    return this.productForm.controls[controlName].hasError(errorName);
  }

  getData() { }

 /* Update form */
  updateProductForm(){
    this.editProductForm = this.fb.group({
      product_name: ['', [Validators.required]],
      weight: ['', [Validators.required]],
      price: ['', [Validators.required]],
      in_stock: ['Yes'],
    })
  }

 updateProduct() {
  var id = this.actRoute.snapshot.paramMap.get('id');
  if(window.confirm('Are you sure you wanna update?')){
      this.productApi.UpdateProduct(id, this.editProductForm.value);
    this.router.navigate(['home']);
  }
}
//Dropzone 
toggleHover(event: boolean) {
  this.isHovering = event;
}

startUpload(event: FileList) {
  // The File object
  const file = event.item(0)

  // Client-side validation example
  if (file.type.split('/')[0] !== 'image') { 
    console.error('unsupported file type :( ')
    return;
  }

  // The storage path
  const path = `test/${new Date().getTime()}_${file.name}`;

  // Totally optional metadata
  const customMetadata = { app: 'My AngularFire-powered PWA!' };

  // The main task
  this.task = this.storage.upload(path, file, { customMetadata })

  // Progress monitoring
  this.percentage = this.task.percentageChanges();
  this.snapshot   = this.task.snapshotChanges();

  // The file's download URL

this.snapshot.pipe(finalize(() => this.downloadURL = this.storage.ref(path).getDownloadURL())).subscribe();this.service.insertImageDetails(this.downloadURL) // And this one to actually grab the URL from the Ref

}

// Determines if the upload task is active
isActive(snapshot) {
  return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes
}

showPreview(event:any){
  if(event.target.files && event.target.files[0]){
    const reader = new FileReader();
    reader.onload=(e:any) => this.imageSrc= e.target.result;
    reader.readAsDataURL(event.target.files[0]);
    this.selectedImage= event.target.files[0];
  }
  else{
    this.imageSrc= '/assets/StandardUploadImagesImage.png';
    this.selectedImage= null;
  }
}

onSubmit(formValue){
  this.isSubmitted=true;
  if(this.formTemplate.valid){
    let filePath=`${formValue.category}${this.selectedImage.name}_${new Date().getTime}`
    const fileRef= this.storage.ref(filePath);

    this.storage.upload(filePath,this.selectedImage).snapshotChanges().pipe(finalize(()=>{
      fileRef.getDownloadURL().subscribe((url)=>{
        formValue['imageUrl']= url;
        this.service.insertImageDetails(formValue);
        this.resetForm();
      })
    }))
      .subscribe();
  }

}

get formControls(){
  return this.formTemplate['controls']
}

resetForm(){
  this.formTemplate.reset();
  this.formTemplate.setValue({
    product_name:'',
    price:'',
    imageUrl:'',
    category:'Uhr'
  });
  this.imageSrc='/assets/StandardUploadImagesImage.png';

  this.isSubmitted=false;

  this.selectedImage=null;

}

}





