import { Component, OnInit,ElementRef,HostListener } from '@angular/core';
import { AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-reusable-button',
  templateUrl: './reusable-button.component.html',
  styleUrls: ['./reusable-button.component.css']
})
export class ReusableButtonComponent implements AfterViewInit {


  // instances for the gradient hover effect on a reusable button
  isGradientVisible = false;
  gradientTop: number;
  gradientLeft: number;
//instance to calculate the size because diffrent buttons might be differ in size
  gradientRadius: number;

  constructor(public el: ElementRef<HTMLElement>) { }

  @HostListener('mouseenter')
  onMouseEnter() {
    this.isGradientVisible = true;
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.isGradientVisible = false;
  }

  @HostListener('mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    this.gradientLeft = event.pageX - this.el.nativeElement.offsetLeft;
    this.gradientTop = event.pageY - this.el.nativeElement.offsetTop;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {  
    this.gradientRadius = this.el.nativeElement.getBoundingClientRect().width;
  }

  // applying the styles to the gradient with the use of ngStyle
  get gradientStyle() {
    const top = this.gradientTop;
    const left = this.gradientLeft;
    const gradientRadius = this.isGradientVisible ? this.gradientRadius : 0;
    
    return {
      'height.px': gradientRadius,
      'width.px': gradientRadius,
      'top.px': top,
      'left.px': left
    };
  }

}
