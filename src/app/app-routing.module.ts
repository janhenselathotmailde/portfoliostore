import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './shared/about-us/about-us.component';
import { SettingsComponent } from './settings/settings.component';
import { HeaderComponent } from './shared/header/header.component';
import { SetproductComponent } from './admin/setproduct.component';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { HomeComponent } from './home/home.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component'
import { ReusableButtonComponent } from './reusable-button/reusable-button.component';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';



const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'home', component: HomeComponent },
  { path: 'shoppingCart', component: ShoppingCartComponent },
  { path: 'button', component: ReusableButtonComponent },



  { path: 'aboutus', component: AboutUsComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'setproduct', component: SetproductComponent },
  { path: '**', redirectTo: '/login', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }